﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C5._1
{
    class Program
    {
        static void Main(string[] args)
        {
            
            int[] array = new int[80];
            Console.WriteLine("Введите количество символов");
            int N = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите символы");
            for (int i = 0; i < N; i++) array[i] = int.Parse(Console.ReadLine());
            int k = 0;
            for (int i = 0; i < N / 2; i++) if (array[i] != array[N - i + 1]) k++;
            Console.WriteLine(k);
            Console.ReadKey();
        }
    }
}
