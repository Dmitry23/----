﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите количество сторон:");
            float n = float.Parse(Console.ReadLine());
            Console.WriteLine("Введите значение радиуса:");
            float R = float.Parse(Console.ReadLine());
            var a = (2 * R * Math.Tan(180 / n));
            var P = (a * n);
            Console.WriteLine("P={0}", a, P);
            Console.ReadKey();

        }
    }
}
