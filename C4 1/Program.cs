﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4_1
{
    class Program
    {
        static void Main(string[] args)
        {
            var nums = new double[100];
            nums[0] = 0.3;
            nums[1] = -0.3;

            double max = nums[0];
            for (int i = 3; i <= 100; i++)
            {
                nums[i - 1] = i + Math.Sin(nums[i - 3]); 
                if (nums[i - 1] % 1 > max % 1) max = nums[i - 1];
            }
            Console.WriteLine(max);
            Console.ReadKey();
        }
    }
}
