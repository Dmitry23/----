﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C8._1
{
    class Program
    {
        static void Main(string[] args)
        { Console.WriteLine("Введите n: ");
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите m: ");
            int m = int.Parse(Console.ReadLine());
            int[,] mas = new int[n,m];
         Random random = new Random();
         int i, j;
         for (i = 0; i < n; i++)
             for (j = 0; j < m; j++)
             {
                 mas[i, j] = random.Next(0, 100);
             }
        
         Console.WriteLine("Вывод неотсортированной матрицы:");
         for (i = 0; i < n; i++)
         {
             for (j = 0; j < m; j++)
             {
                 Console.Write(mas[i, j] + " ");
             }
             Console.WriteLine();
         }
         Console.WriteLine();
          
          for (i = 0; i < n; i++)
          {
              bool flag = true;
              while (flag)
              {
                  flag = false;
                  for (j = 1; j < m; j++)
                  {
                      if (mas[i, j - 1] > mas[i, j])
                      {
                          int temp = mas[i, j - 1];
                          mas[i, j - 1] = mas[i, j];
                          mas[i, j] = temp;
                          flag = true;
                      }
                  } 
              }
          }
          Console.WriteLine();
          Console.WriteLine();
          Console.WriteLine("Вывод отсортированной матрицы:");
          for (i = 0; i < n; i++)
          {
              for (j = 0; j < m; j++)
              {
                  Console.Write(mas[i, j] + " ");
              }
              Console.WriteLine();
          }


         Console.ReadLine();




                }
        }
    }
        
    

