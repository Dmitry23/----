﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C9._1
{
    class Program
    {
        static int SumDel(int x)
        {
            int res = 0;
            for (int i = 1; i <= x / 2; i++)
                if (x % i == 0) res += i;
            return res;
        }
        
            
        static void Main(string[] args)
        {
            int n = 1, m = 10000;
            Console.WriteLine("N=");
            n = int.Parse(Console.ReadLine());
            Console.WriteLine("M=");
            m = int.Parse(Console.ReadLine());
            for (int i = n; i <= m; i++)
            {
                if (SumDel(SumDel(i))==i)
                    Console.WriteLine(i + " и " + SumDel(i) + "\a");
            }
            Console.ReadKey();
        }
    }
}
